var width = window.innerWidth
var height = window.innerHeight

function windowResized() {
  width = windowWidth
  height = windowHeight
  resizeCanvas(width, height)
  background(0,0,0)
}

function preload(){
  sound = loadSound('assets/startup.mp3');
}


function setup(){
  background(0,0,0)
  let cnv = createCanvas(window.innerWidth, window.innerHeight);
  cnv.mouseClicked(togglePlay);
  fft = new p5.FFT();
  sound.amp(1);
  colorMode(RGB, 255)
  textSize(48)
  textFont('Consolas')
  soundLength = sound.duration()
  frameRate(30)
}

var t = 0
var hue = 0
var letterlist = [
'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
var letterfreq = [
'E','T','A','O','I','N','S','R','H','D','L','U','C','M','F','Y','W','G','P','B','V','K','X','Q','J','Z']

function draw(){
  if (t == 0){
    background(0,0,0);
  }
  if (sound.isPlaying()){
    noStroke()
    fill(0)
    rect(0,0,width,height)
    //stroke(0)
    let speclen = 64
    let spectrum = fft.analyze(speclen);
    let wavePoints = []

    for (let i = 0; i < speclen; i++){
      if (i % 2 == 0) {
        fill(255)
      } else {
        fill(0)
      }
      let x = map(i, 0, speclen-1, width/2, 7 * width/8);
      let y = 2 * height/3
      let noise = map(spectrum[i], 0, 255, 0, 10)
      let rx = random(x- noise/4, x + noise/4)
      let ry = random(y - noise/4, y + noise/4)
      let letterindex = int(map(spectrum[i], 0, 255, 0, 25, true));
      let size = random(52 - noise, 52 + noise)
      textSize(size)
      text(letterlist[letterindex], rx, ry)

      let wy = random(height/3 - 2, height/3 + 2)
      let dy = wy + spectrum[i]
      append(wavePoints, [rx, dy])
    }

    noFill()
    stroke(255)
    for (let i = 0; i < speclen-1; i++){
      strokeWeight(random(2,4))
      line(wavePoints[i][0], wavePoints[i][1], wavePoints[i+1][0], wavePoints[i+1][1])
    }
    let xDelta = map(1, 0, speclen-1, width/2, 7 * width/8) - width/2
    line(wavePoints[speclen-1][0], wavePoints[speclen-1][1], wavePoints[speclen-1][0] + xDelta, random(wavePoints[speclen-1][1] - 2, wavePoints[speclen-1][1] + 2))

    let sizeInner = map(spectrum[int(speclen/4)], 0, 255, 10, 50)
    let sizeOuter = map(spectrum[int(3 * speclen/4)], 0, 255, 500, 650)
    let center = [width/4, height/2]
    circle(center[0], center[1], sizeOuter)
    circle(center[0], center[1], sizeInner)

    fill(255)
    stroke(0)
    strokeWeight(5)
    let angleNoise = map(spectrum[int(speclen/2)], 0, 255, 0, 1)
    let angle = map(sound.currentTime(), 0, soundLength, 0, 2*PI) + angleNoise/2
    if (angle <= 2*PI) {
      arc(center[0], center[1], sizeOuter, sizeOuter, PI, PI + angle)
    }
    t += 0.01
  }
}

function togglePlay() {
  if (sound.isPlaying()) {
    sound.pause();
  } else {
    sound.loop();
  }
}
